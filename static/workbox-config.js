module.exports = {
  "globDirectory": ".",
  "globPatterns": [
    "**/*.{css,js,json,html,png,woff,woff2}"
  ],
  "swDest": "sw.js",
  "swSrc": "sw_template.js"
};
