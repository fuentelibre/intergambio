const System = window.System || {}

import "@anydown/vue-spreadsheet-lite/dist/vue-spreadsheet-lite.cjs.css"
import "xp.css/dist/98.css"

import Vue from 'vue'
import VueSpreadsheetLite from '@anydown/vue-spreadsheet-lite'
import FS from '@isomorphic-git/lightning-fs'
import Gun from 'gun/gun'
import SEA from 'gun/sea'
import NTS from 'gun/nts'
import 'gun/lib/webrtc'

import VueGun from 'vue-gun'

Vue.use(VueGun, {
    gun: new Gun(['https://peer-mercalocal.fuentelibre.org/gun'])
})
Vue.component('vue-spreadsheet-lite', VueSpreadsheetLite)

const fs = new FS("intergambio_fs")

startUp(Vue, fs)

const root = new Vue({ el:"#intergambio",
          data: {
          }
	})

if ( window.guy )
	guy.init(()=>{
            window.server=self
            guy.emitMe('server-ready')
        })

if ('serviceWorker' in navigator) {
  console.log('Installing Service Worker')
  navigator.serviceWorker.register('/service-worker.js')
}

window.root = root
window.fs = fs
window.SEA = SEA
